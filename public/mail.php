<?php

$to = 'stanislovas.janonis@gmail.com';

$subject = 'Wash order';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'To: Admin <stanislovas.janonis@gmail.com>' . "\r\n";
$headers .= 'From: Washing services <wash@example.com>' . "\r\n";


if(!isset($_POST['M']) && !isset($_POST['W']) && !isset($_POST['K']) && !isset($_POST['H'])) die();

if(isset($_POST['M'])) $M = $_POST['M'];
if(isset($_POST['W'])) $W = $_POST['W'];
if(isset($_POST['K'])) $K = $_POST['K'];
if(isset($_POST['H'])) $H = $_POST['H'];

if(!isset($_POST['address_line_1'])) echo 'no address line 1';
if(!isset($_POST['city'])) echo 'no city';
if(!isset($_POST['email']) && !isset($_POST['phone'])) echo 'no email and phone';

// mail_body
$mail_body = '
<html>
<head>
  <title>New order</title>
  <style>
    table{
      border-collapse: collapse;
      border: 1px solid black;
    }
    table td{
      min-width: 60px;
      border: 1px solid black;
  	}
  </style>
</head>
<body>
  <table style="width: 800px">
     <tr><td>Pick up time</td><td>' .             $_POST['pick_up_time'] . '</td></tr>
     <tr><td>Laundry return time</td><td>' .      $_POST['laundry_return_time'] . '</td></tr>
     <tr><td>Dry-cleaning return time</td><td>' . $_POST['dry_cleaning_return_time'] . '</td></tr>
     <tr><td>Address line 1</td><td>' .           $_POST['address_line_1'] . '</td></tr>
     <tr><td>Address line 2</td><td>' .           $_POST['address_line_2'] . '</td></tr>
     <tr><td>City</td><td>' .                     $_POST['city'] . '</td></tr>
     <tr><td>State</td><td>' .                    $_POST['state'] . '</td></tr>
     <tr><td>Post code</td><td>' .                $_POST['post_code'] . '</td></tr>
     <tr><td>Name</td><td>' .                     $_POST['name'] . '</td></tr>
     <tr><td>E-mail</td><td>' .                   $_POST['email'] . '</td></tr>
     <tr><td>Phone</td><td>' .                    $_POST['phone'] . '</td></tr>
  </table>
  <table border="1" style="width: 800px">';

if(isset($M)) {
	$mail_body .= '<tr><td><b>Men</b></td></tr><tr><td>Name</td><td>Count</td><td>Dry-washing</td><td>Iron</td><td>Information</td></tr>';
	ListItems($M);
}
if(isset($W)) {
	$mail_body .= '<tr><td><b>Women</b></td></tr><tr><td>Name</td><td>Count</td><td>Dry-washing</td><td>Iron</td><td>Information</td></tr>';
	ListItems($W);
}
if(isset($K)) {
	$mail_body .= '<tr><td><b>Kids</b></td></tr><tr><td>Name</td><td>Count</td><td>Dry-washing</td><td>Iron</td><td>Information</td></tr>';
	ListItems($K);
}
if(isset($H)) {
	$mail_body .= '<tr><td><b>House</b></td></tr><tr><td>Name</td><td>Count</td><td>Dry-washing</td><td>Iron</td><td>Information</td></tr>';
	ListItems($H);
}

$mail_body .= '
  </table>
</body>
</html>
';

function ListItems($X) {
	global $mail_body;

	for($i = 0; $i < count($X['id']); $i++) {
		$mail_body .= '
		<tr>
			<td><i>' . $X['id'][$i] . '</i></td>
			<td><i>' . $X['a'][$i] . '</i></td>
			<td><i>' . (($X['d'][$i] === 0) ? ('false') : ('true')) . '</i></td>
			<td><i>' . (($X['i'][$i] === 0) ? ('false') : ('true')) . '</i></td>
			<td><i>' . $X['m'][$i] . '</i></td>
		</tr>
		';
	}
}

// Mail it
$st = mail($to, $subject, $mail_body, $headers);

echo $st . '<br>' . $mail_body;

?>