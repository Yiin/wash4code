/*
	Laiku pasirinkimas:

	Drop-off laikas dry-cleaning ir laundry turi buti skirtingas. 
*/

var stepID = 1;

// STEP ONE
var men = {type: [], amount: [], dry: []};
var women = {type: [], amount: [], dry: []};
var kids = {type: [], amount: [], dry: []};
var house = {type: [], amount: [], dry: []};
var activeTab = '#men';

// because reasons
var M = {id: [], a: [], i: [], d: [], m: []};
var W = {id: [], a: [], i: [], d: [], m: []};
var K = {id: [], a: [], i: [], d: [], m: []};
var H = {id: [], a: [], i: [], d: [], m: []};

var men_cats = [
	"Underwear",
	"T-Shirt",
	"T-Shirt (full sleeve)",
	"T-Shirt (without sleeve)",
	"Shirt",
	"Sweat Shirt",
	"Jeans",
	"Shorts",
	"Trouser",
	"Hoodie",
	"Hoodie (with zipper)",
	"Sweater",
	"Sweater (half)",
	"Kurta",
	"Pyjama",
	"Dhoti",
	"Socks",
	"Jacket",
	"Jacket (without sleeves)",
	"Bathing gown",
	"Towel",
	"Coat",
	"Pant",
	"Tie",
	"Track Suit (upper and bottom)",
	"Blazer",
	"Full Suit (3 piece)",
	"Safari Suit",
	"Shawl"
];
var women_cats = [
	"Underwear",
	"T-Shirt (top)",
	"T-Shirt (full sleeve)",
	"T-Shirt (without sleeve)",
	"Shirt",
	"Sweat Shirt",
	"Jeans",
	"Shorts",
	"Trouser",
	"Hoodie",
	"Hoodie (with zipper)",
	"Lady Kurta (embroidery)",
	"Lady Kurta (silk)",
	"Pyjama",
	"Sweater",
	"Socks",
	"Jacket",
	"Jacket (without sleeves)",
	"Bathing gown",
	"Towel",
	"Coat",
	"Pant",
	"Tie",
	"Track Suit (upper and bottom)",
	"Blazer",
	"Saree (Silk)",
	"Saree (Plain)",
	"Blouse (embroidery)",
	"Blouse (plain)",
	"Peticot",
	"Salwar",
	"Dupatta (heavy)",
	"Dupatta (light)",
	"Lady Suit (2 piece)",
	"Capri",
	"Dressing gown",
	"Hand Bag (small)",
	"Hand Bag (large)",
	"Nighty (2 piece)",
	"Skirt ",
	"Stole (plain)",
	"Stole (pashmina)"
];
var kids_cats = [
	"Upper",
	"Lower",
	"Jacket",
	"Coat"
];
var house_cats = [
	"Duvet (Blanket)",
	"Sofa Cover (4 seated sofa)",
	"Sofa Cover (3 seated sofa)",
	"Sofa Cover (2 seated sofa)",
	"Sofa Cover (1 seated sofa)",
	"Curtain",
	"Bed sheet (double bed)",
	"Bed sheet (single bed)",
	"Stuffed toys",
	"Travel bag"
];

// STEP FOURTH
// [weight(g), drycleaning($)]
var MLD = [
	[70, 0],
	[260, 100],
	[300, 100],
	[200, 100],
	[260, 100],
	[155, 140],
	[610, 135],
	[500, 75],
	[250, 100],
	[520, 180],
	[740, 180],
	[400, 180],
	[280, 150],
	[300, 240],
	[200, 70],
	[150, 115],
	[45, 0],
	[990, 250],
	[600, 220],
	[970, 200],
	[900, 100],
	[775, 260],
	[308, 150],
	[50, 70],
	[500, 220],
	[450, 260],
	[0, 300],
	[0, 270],
	[200, 150]
];

var WLD = [
	[70, 0],
	[260, 100],
	[300, 100],
	[200, 100],
	[260, 100],
	[155, 140],
	[610, 135],
	[500, 75],
	[250, 100],
	[520, 180],
	[740, 180],
	[300, 270],
	[250, 160],
	[200, 70],
	[400, 180],
	[45, 0],
	[990, 250],
	[600, 220],
	[970, 200],
	[900, 100],
	[775, 260],
	[308, 150],
	[50, 70],
	[500, 220],
	[450, 260],
	[250, 220],
	[200, 150],
	[150, 120],
	[100, 60],
	[100, 90],
	[100, 100],
	[140, 180],
	[100, 120],
	[500, 220],
	[300, 75],
	[0, 220],
	[0, 80],
	[0, 120],
	[500, 210],
	[350, 120],
	[200, 130],
	[250, 240]
];

var KLD = [
	[100,100],
	[100,100],
	[300,170],
	[300,190]
];

var HLD = [
	[4000,350],
	[1000,300],
	[900,290],
	[800,280],
	[700,270],
	[4000,500],
	[1600,300],
	[1000,280],
	[2000,300]
];
var IroningPrice = 9;
var address_line_1;
var address_line_2;
var city;
var state;
var post_code;
var name;
var email;
var phone;

// STEP FIVE
var pick_up_time = '';
var laundry_return_time = '';
var dry_cleaning_return_time = '';
var total_price = 0;

$(document).ready(function() {

	function LoadStep(step) {
		if(step == 5) {
			address_line_1 = $('#address_street1').val();
			address_line_2 = $('#address_street2').val();
			city = $('#city').val();
			state = $('#state').val();
			post_code = $('#zipcode').val();
			name = $('#customer_name').val();
			email = $('#customer_email').val();
			phone = $('#customer_phone').val();
		}
		$('#main-form').children().remove();

		switch(step) {
			case 1: {
				$('#extra-info-pane-icon span').addClass('active');
				$('#pick-clothes-pane-icon span').removeClass('active');

				$('#pick-clothes-pane-icon span').parent('li').addClass('active access-approved');
				$('#extra-info-pane-icon span').parent('li').removeClass('active access-approved');

				var html = '<div class="form-group">';
				html += '<ul class="nav nav-tabs" role="tablist" id="laundry-types">';
				html += '<li class="active" id="men"><a class="nav-tab" unselectable="on" onselectstart="return false;" onmousedown="return false;">Men <span class="badge">0</span></a></li>';
				html += '<li id="women"><a class="nav-tab" unselectable="on" onselectstart="return false;" onmousedown="return false;">Women <span class="badge">0</span></a></li>';
				html += '<li id="kids"><a class="nav-tab" unselectable="on" onselectstart="return false;" onmousedown="return false;">Kids <span class="badge">0</span></a></li>';
				html += '<li id="house"><a class="nav-tab" unselectable="on" onselectstart="return false;" onmousedown="return false;">House <span class="badge">0</span></a></li>';
				html += '</ul>';
				html += '</div>';
				html += '<div id="items-list">';
				html += '</div>';
				$('#main-form').append(html);

				$('#men').attr('class', '');
				$(activeTab).attr('class', 'active');

				listItems();
				break;
			}
			case 2: {
				$('#pick-clothes-pane-icon span').removeClass('active');
				$('#iron-pane-icon span').removeClass('active');
				$('#extra-info-pane-icon span').addClass('active');

				$('#pick-clothes-pane-icon span').parent('li').removeClass('active access-approved');
				$('#iron-pane-icon span').parent('li').removeClass('active access-approved');
				$('#extra-info-pane-icon span').parent('li').addClass('active access-approved');

				M = {id: [], a: [], i: [], d: [], m: []};
				W = {id: [], a: [], i: [], d: [], m: []};
				K = {id: [], a: [], i: [], d: [], m: []};
				H = {id: [], a: [], i: [], d: [], m: []};

				for (var i = 0; i < men.type.length; i++) {
					if(men.type[i] > 0 && men.amount[i] > 0) {
						M.id.push(men.type[i] - 1);
						M.a.push(men.amount[i]);
						M.d.push(men.dry[i])
						console.log(men.dry[i]);
						M.i.push((men.dry[i] == false) ? 0 : 1);
					}
				};
				for (var i = 0; i < women.type.length; i++) {
					if(women.type[i] > 0 && women.amount[i] > 0) {
						W.id.push(women.type[i] - 1);
						W.a.push(women.amount[i]);
						W.d.push(women.dry[i])
						W.i.push((women.dry[i] == false) ? 0 : 1);
					}
				};
				for (var i = 0; i < kids.type.length; i++) {
					if(kids.type[i] > 0 && kids.amount[i] > 0) {
						K.id.push(kids.type[i] - 1);
						K.a.push(kids.amount[i]);
						K.d.push(kids.dry[i])
						K.i.push((kids.dry[i] == false) ? 0 : 1);
					}
				};
				for (var i = 0; i < house.type.length; i++) {
					if(house.type[i] > 0 && house.amount[i] > 0) {
						H.id.push(house.type[i] - 1);
						H.a.push(house.amount[i]);
						H.d.push(house.dry[i])
						H.i.push((house.dry[i] == false) ? 0 : 1);
					}
				};

				var html = '<div class="form-group">';
				html += '<ul>';

				for (var i = 0; i < M.id.length; i++) {
					html += '<li><div class="input-group input-group-sm" style="width: 800px;"><span class="input-group-addon" style="width: 200px; padding: 0px;">' + men_cats[M.id[i]] + '</span><input class="form-control" type="text"></input></div></li>';
				}
				for (var i = 0; i < W.id.length; i++) {
					html += '<li><div class="input-group input-group-sm" style="width: 800px;"><span class="input-group-addon" style="width: 200px; padding: 0px;">' + women_cats[W.id[i]] + '</span><input class="form-control" type="text"></input></div></li>';
				}
				for (var i = 0; i < K.id.length; i++) {
					html += '<li><div class="input-group input-group-sm" style="width: 800px;"><span class="input-group-addon" style="width: 200px; padding: 0px;">' + kids_cats[K.id[i]] + '</span><input class="form-control" type="text"></input></div></li>';
				}
				for (var i = 0; i < H.id.length; i++) {
					html += '<li><div class="input-group input-group-sm" style="width: 800px;"><span class="input-group-addon" style="width: 200px; padding: 0px;">' + house_cats[H.id[i]] + '</span><input class="form-control" type="text"></input></div></li>';
				}
				html += '</ul></div>';

				$('#main-form').append(html);
				break;
			}
			case 3: {

				$('#contact-pane-icon span').removeClass('active');
				$('#extra-info-pane-icon span').removeClass('active');
				$('#iron-pane-icon span').addClass('active');

				$('#contact-pane-icon span').parent('li').removeClass('active access-approved');
				$('#extra-info-pane-icon span').parent('li').removeClass('active access-approved');
				$('#iron-pane-icon span').parent('li').attr('class', 'progress-bars step-3 active access-approved');

				// Step 3. Pasirenka kuriuos is irasytu rubu nori kad islygintu 
				// arba tiesiog paspaudzia "Iron All" ir pasirenka kad islygintu visus.

				var html = '<div class="form-group">';
				html += '<label><input type="checkbox" id="check-all"> Iron all</label></input>';
				html += '<ul>';

				for (var i = 0; i < M.id.length; i++) {
					if(M.d[i] == false) {
						html += '<li><label><input type="checkbox" class="single-check"> Iron it</input></label><span class="checkbox-text">' + men_cats[M.id[i]] + '</span></li>';
					}
				}
				for (var i = 0; i < W.id.length; i++) {
					if(W.d[i] == false) {
						html += '<li><label><input type="checkbox" class="single-check"> Iron it</input></label><span class="checkbox-text">' + women_cats[W.id[i]] + '</span></li>';
					}
				}
				for (var i = 0; i < K.id.length; i++) {
					if(K.d[i] == false) {
						html += '<li><label><input type="checkbox" class="single-check"> Iron it</input></label><span class="checkbox-text">' + kids_cats[K.id[i]] + '</span></li>';
					}
				}
				for (var i = 0; i < H.id.length; i++) {
					if(H.d[i] == false) {
						html += '<li><label><input type="checkbox" class="single-check"> Iron it</input></label><span class="checkbox-text">' + house_cats[H.id[i]] + '</span></li>';
					}
				}
				html += '</ul></div>';

				$('#main-form').append(html);
				break;
			}
			case 4: {
				$('#date-pane-icon span').removeClass('active');
				$('#iron-pane-icon span').removeClass('active');
				$('#contact-pane-icon span').addClass('active');

				$('#date-pane-icon span').parent('li').removeClass('active access-approved');
				$('#iron-pane-icon span').parent('li').removeClass('active access-approved');
				$('#contact-pane-icon span').parent('li').addClass('active access-approved');
				
				// Total Price: Reikia parodyt tik kiek kainuos lyginimas, dry-cleaning ir laundry. 
				// Nereikia kiekvieno item atskirai. (lyginimo kaina yra 9 INR uz viena item. Tai pvz 5 shirts butu 9 x 5.)

				$.get('/assets/html/step-4.html', function(data) {
					$('#main-form').append(data);

					$('#total-price').html('Total price: <b>' + TotalPrice() + '</b>');

					var html = '<table style="width: 100%;">'; // tr row td col

					html += '<tr>';
					html += '<td>Ironing</td>';
					html += '<td>' + PriceOf('ironing') + ' INR</td>';
					html += '</tr>';
					html += '<tr>';

					html += '<td>Dry-cleaning</td>';
					html += '<td>' + PriceOf('dry-cleaning') + ' kg.</td>';
					html += '</tr>';
					html += '<tr>';

					html += '<td>Laundry</td>';
					html += '<td>' + PriceOf('laundry') + ' kg.</td>';
					html += '</tr>';
					
					html += '</table">';

					$('#list-price').html(html);
				});

				break;
			}
			case 5: {
				$('#pay-pane-icon span').removeClass('active');
				$('#contact-pane-icon span').removeClass('active');
				$('#date-pane-icon span').addClass('active');


				$('#pay-pane-icon span').parent('li').removeClass('active access-approved');
				$('#contact-pane-icon span').parent('li').removeClass('active access-approved');
				$('#date-pane-icon span').parent('li').addClass('active access-approved');

				var html = '<div class="form-group">';
				html += '<table style="width: 500px;">';
				html += '<tr><td><h6>Pick-up time </h6></td><td><input id="pickup-time" type="text"></td></tr>';

				html += '<tr><td><h6>Laundry return time<b>*</b> </h6></td><td><input id="laundry-return-time" type="text"></td></tr>';

				html += '<tr><td><h6>Dry-cleaning return time<b>**</b> </h6></td><td><input id="dry-cleaning-return-time" type="text"></td></tr>';
				html += '</table>';
				html += '<div class="container">';
				html += '<p><b>*</b> Laundry can be returned after 5 hours<br>';
				html += '<b>**</b> Dry-cleaning will take <b>minimum</b> 24 hours.</p></div>';
				html += '</div>';

				$('#main-form').append(html);

				InitTimePickers();

				break;
			}
			case 6: {
				$('#date-pane-icon span').parent('li').removeClass('active');
				$('#pay-pane-icon span').parent('li').addClass('active');

				$('#date-pane-icon span').parent('li').removeClass('active access-approved');
				$('#pay-pane-icon span').parent('li').addClass('active access-approved');

				var html = '<div class="form-group">';
				html += '<div class="radio disabled"><label><input type="radio" name="optradio" disabled>Pay via credit/debit card (coming soon)</label></div>';
				html += '<div class="radio"><label><input type="radio" name="optradio" checked>Pay at pickup</label></div>';

				$('#main-form').append(html);
				
				break;
			}
			/*
			case 7: {
				$('#pay-pane-icon span').removeClass('active');
				$('#review-order-pane-icon span').addClass('active');

				$('#pay-pane-icon span').parent('li').attr('class', 'progress-bars step-6');
				$('#review-order-pane-icon span').parent('li').attr('class', 'progress-bars step-7 active access-approved');
				
				break;
			}
			*/
		}
	}

	$('#next-step').on('click', function(event) {
		$('#next-step').prop('disabled', true);
		$('#prev-step').prop('disabled', true);
		setTimeout(function(){ $('#next-step').prop('disabled', false) }, 300);
		setTimeout(function(){ $('#prev-step').prop('disabled', false) }, 300);

		if(stepID != -1) {
			if(++stepID > 6) {
				stepID = -1;

				total_price = TotalPrice();

				for (var i = 0; i < M.id.length; i++) {
					M.id[i] = men_cats[M.id[i]];
				};
				for (var i = 0; i < W.id.length; i++) {
					W.id[i] = women_cats[W.id[i]];
				};
				for (var i = 0; i < K.id.length; i++) {
					K.id[i] = kids_cats[K.id[i]];
				};
				for (var i = 0; i < H.id.length; i++) {
					H.id[i] = house_cats[H.id[i]];
				};
				if(city === undefined) city = '';
				if(name === undefined) name = '';
				if(email === undefined) email = '';
				if(phone === undefined) phone = '';
				if(state === undefined) state = '';
				if(post_code === undefined) post_code = '';
				if(total_price === undefined) total_price = '';
				if(address_line_1 === undefined) address_line_1 = '';
				if(pick_up_time === undefined) pick_up_time = '';
				if(address_line_2 === undefined) address_line_2 = '';
				if(laundry_return_time === undefined) laundry_return_time = '';
				if(dry_cleaning_return_time === undefined) dry_cleaning_return_time = '';

				$.ajax({
				  type: "POST",
				  url: "mail.php",
                  data: { 
                                    city: city,
                                    name: name,
                                   email: email,
                                   phone: phone,
                                   state: state,
                               post_code: post_code,
                              M: M, W: W, K: K, H: H, 
                          address_line_1: address_line_1,
                             total_price: total_price,
                            pick_up_time: pick_up_time,
                          address_line_2: address_line_2,
                     laundry_return_time: laundry_return_time,
                dry_cleaning_return_time: dry_cleaning_return_time
                  }
                })
				.done(function( msg ) {
					prompt(msg);
				});
			}
		}

		LoadStep(stepID);
	});
	$('#prev-step').on('click', function(event) {
		$('#next-step').prop('disabled', true);
		$('#prev-step').prop('disabled', true);
		setTimeout(function(){ $('#next-step').prop('disabled', false) }, 300);
		setTimeout(function(){ $('#prev-step').prop('disabled', false) }, 300);

		if(--stepID < 1) {
			stepID = 1;
		}

		LoadStep(stepID);
	});

	// STEP SIX
	$('#main-form').on('click', '#btn-pay-at-pickup', function() {
		$(this).addClass('active');
	});

	// STEP FIVE
	function InitTimePickers() {
		$('#pickup-time').datetimepicker({
			onChangeDateTime:function(dp,$input){
		    	pick_up_time = $input.val();
			},
			minDate:'-1970/01/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
			minTime:'8:00',
			maxTime:'21:00'
		});
		$('#laundry-return-time').datetimepicker({
			onChangeDateTime:function(dp,$input){
		    	laundry_return_time = $input.val();
			},
			formatDate:'Y/m/d',
			minDate:'-1970/01/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
			minTime:'8:00',
			maxTime:'21:00'
		});
		$('#dry-cleaning-return-time').datetimepicker({
			onChangeDateTime:function(dp,$input){
		    	dry_cleaning_return_time = $input.val();
			},
			formatDate:'Y/m/d',
			minDate:'+1970/01/02',//tommorow
			minTime:'8:00',
			maxTime:'21:00'
		});
	}

	// STEP FOUR
	function CalcWeight(X, XLD, i) {
		var ret = 0;
			ret += XLD[X.id[i]][0] * X.a[i];
		return ret / 1000;
	}
	function PriceOf(type) {
		var price = 0;

		if(type == 'ironing') {
			for (var i = 0; i < M.id.length; i++) {
				if(M.i[i] == true) {
					price += IroningPrice * M.a[i];
				}
			};
			for (var i = 0; i < W.id.length; i++) {
				if(W.i[i] == true) {
					price += IroningPrice * W.a[i];
				}
			};
			for (var i = 0; i < K.id.length; i++) {
				if(K.i[i] == true) {
					price += IroningPrice * K.a[i];
				}
			};
			for (var i = 0; i < H.id.length; i++) {
				if(H.i[i] == true) {
					price += IroningPrice * H.a[i];
				}
			};
		}
		else if(type == 'washing') {
			price = 250;
			var current_weight = 4000;
			var weight = PriceOf('dry-cleaning') + PriceOf('laundry');
			while(current_weight < weight) {
				current_weight += 1000;
				price += 50;
			}
		}
		else if(type == 'dry-cleaning') {
			for (var i = 0; i < M.id.length; i++) {
				if(M.d[i] == true) {
					price += CalcWeight(M, MLD, i);
				}
			};
			for (var i = 0; i < W.id.length; i++) {
				if(W.d[i] == true) {
					price += CalcWeight(W, WLD, i);
				}
			};
			for (var i = 0; i < K.id.length; i++) {
				if(K.d[i] == true) {
					price += CalcWeight(K, KLD, i);
				}
			};
			for (var i = 0; i < H.id.length; i++) {
				if(H.d[i] == true) {
					price += CalcWeight(H, HLD, i);
				}
			};
		}
		else if(type == 'laundry') {
			for (var i = 0; i < M.id.length; i++) {
				if(M.d[i] == false) {
					price += CalcWeight(M, MLD, i);
				}
			};
			for (var i = 0; i < W.id.length; i++) {
				if(W.d[i] == false) {
					price += CalcWeight(W, WLD, i);
				}
			};
			for (var i = 0; i < K.id.length; i++) {
				if(K.d[i] == false) {
					price += CalcWeight(K, KLD, i);
				}
			};
			for (var i = 0; i < H.id.length; i++) {
				if(H.d[i] == false) {
					price += CalcWeight(H, HLD, i);
				}
			};
		}
		
		return price;
	}

	function TotalPrice() {
		return PriceOf('ironing') + PriceOf('washing');
	}

	// STEP THREE

	$('#main-form').on('click', 'input[type="checkbox"]', function() {
		if($(this).attr('id') == 'check-all') {
			var check = $(this).prop('checked');

			$('.single-check').each(function(index) {
				if($(this).prop('checked') != check)
					$(this).trigger('click');
			});
		}
		else {
			var idx = $('.single-check').index(this);

			if(idx < M.id.length) {
				M.i[idx] = $(this).prop('checked');
			}
			else if((idx -= M.id.length) < W.id.length) {
				W.i[idx] = $(this).prop('checked');
			}
			else if((idx -= W.id.length) < K.id.length) {
				K.i[idx] = $(this).prop('checked');
			}
			else if((idx -= K.id.length) < H.id.length) {
				H.i[idx] = $(this).prop('checked');
			}
			var check_all = true;
			$('.single-check').each(function(index) {
				if($(this).prop('checked') == false) {
					check_all = false;
				}
			}); 
			$('#check-all').prop('checked', check_all);
		}
	});

	// STEP TWO

	$('#main-form').on('change', '.input-group-sm input', function(event) {
		var idx = $('#main-form .input-group-sm input').index(this);

		if(idx < M.id.length) {
			M.m[idx] = $(this).val();
		}
		else if((idx -= M.id.length) < W.id.length) {
			W.m[idx] = $(this).val();
		}
		else if((idx -= W.id.length) < K.id.length) {
			K.m[idx] = $(this).val();
		}
		else if((idx -= K.id.length) < H.id.length) {
			H.m[idx] = $(this).val();
		}
	});

	// STEP ONE
	function getRowDefaultHTML() {
		var html = '<form><div class="form-group pick-item"><select class="pck-itm">';

		html += '<option value="-1">Select type of clothes</option>';
		switch(activeTab) {
			case '#men': {
				for(var i = 1; i <= men_cats.length; ++i) {
					if(men.type.indexOf(i) == -1) {
						html += '<option value='+i+'>'+men_cats[i-1]+'</option>';
					}
					else {
						html += '<option value='+i+' disabled>'+men_cats[i-1]+'</option>';
					}
				}
				break;
			}
			case '#women': {
				for(var i = 1; i <= women_cats.length; ++i) {
					if(women.type.indexOf(i) == -1) {
						html += '<option value='+i+'>'+women_cats[i-1]+'</option>';
					}
					else {
						html += '<option value='+i+' disabled>'+women_cats[i-1]+'</option>';
					}
				}
				break;
			}
			case '#kids': {
				for(var i = 1; i <= kids_cats.length; ++i) {
					if(kids.type.indexOf(i) == -1) {
						html += '<option value='+i+'>'+kids_cats[i-1]+'</option>';
					}
					else {
						html += '<option value='+i+' disabled>'+kids_cats[i-1]+'</option>';
					}
				}
				break;
			}
			case '#house': {
				for(var i = 1; i <= house_cats.length; ++i) {
					if(house.type.indexOf(i) == -1) {
						html += '<option value='+i+'>'+house_cats[i-1]+'</option>';
					}
					else {
						html += '<option value='+i+' disabled>'+house_cats[i-1]+'</option>';
					}
				}
				break;
			}
		}
		html += '</select>';
		html += '<select class="cnt-clth">';
		html += '<option>Select count of clothes</option>';
		for (var i = 1; i <= 50; i++) {
			html += '<option>' + i + '</option>';
		};
		html += '</select>';
		html += '<div class="row wsh-tp">';
		html += '<div class="col-xs-7"><input name="type" type="radio" value="laundry" checked> Laundry</div>';
		html += '<div class="col-xs-7"><input name="type" type="radio" value="dry-cleaning"> Dry-cleaning</div></div></div></form>';
		return html;
	}

	function TabToVar() {
		switch(activeTab) {
			case '#men': { return men; }
			case '#women': { return women; }
			case '#kids': { return kids; }
			case '#house': { return house; }
		}
		return undefined;
	}

	function listItems() {
		$('#items-list').children('form').remove();

		var list = TabToVar();

		for (var i = 0; i < 3; i++) {
			$('#items-list').append(getRowDefaultHTML());
			if(i < list.type.length) {
				$('#items-list .pick-item:last option[value=' + list.type[i] + ']').prop('selected', true);
				$('#items-list .cnt-clth:last option').eq(list.amount[i]).prop('selected', true);
				if(list.dry[i] == true) {
					$('#items-list input[name="type"][value="dry-cleaning"]').eq(i).prop('checked', true);
				}
				else {
					$('#items-list input[name="type"][value="laundry"]').eq(i).prop('checked', true);
				}
			}
		};

		for (var i = 3; i < list.type.length; i++) {
			$('#items-list').append(getRowDefaultHTML());

			$('#items-list .pick-item:last option[value=' + list.type[i] + ']').prop('selected', true);
			$('#items-list .cnt-clth').last().eq(list.amount[i]).prop('selected', true);
			if(list.dry[i] == true) {
				$('#items-list input[name="type"][value="dry-cleaning"]').eq(i).prop('checked', true);
			}
			else {
				$('#items-list input[name="type"][value="laundry"]').eq(i).prop('checked', true);
			}
		};

		updateStuff();
	}

	function updateStuff() {
		$(activeTab + ' .badge').text(countItems());
		//
	}

	function allUsed() {
		var list = TabToVar();
		var all_slots_are_used = true;
		$('#items-list form').each(function(i) {
			if(((list.type[i] > 0) == false) || ((list.amount[i] > 0) == false)) {
				all_slots_are_used = false;
			}
		});
		return all_slots_are_used;
	}

	function countItems() {
		var count = 0;
		$('#items-list .pick-item').each(function(i) {
			if( ! badValue(i, 'count') && ! badValue(i, 'list')) {
				count++;
			}
		});
		return count;
	}

	function badValue(idx, type) {
		var list = TabToVar();
		if(type === 'count') {
			return (list.amount[idx] > 0) == false;
		}
		else {
			return (list.type[idx] > 0) == false;
		}
	}

	function CountChanged(_this) {
		var idx = $('.cnt-clth').index(_this);
		var list = TabToVar();

		list.amount[idx] = parseInt($(_this).val());
		list.dry[idx] = $('#items-list input[name="type"][value="dry-cleaning"]').eq(idx).prop('checked');

		if(allUsed()) {
			$('#items-list').append(getRowDefaultHTML());
		}
		updateStuff();
	}

	LoadStep(stepID);

	$('#main-form').on('change', '#items-list form', function() {
		var idx = $('#items-list form').index(this);
		var list = TabToVar();

		list.dry[idx] = $('#items-list input[name="type"][value="dry-cleaning"]').eq(idx).prop('checked');
	});
	$('#main-form').on('keyup', '#items-list .cnt-clth', function(event) {
		CountChanged(this);
	})
	$('#main-form').on('change', '#items-list .cnt-clth', function(event) {
		CountChanged(this);
	});

	$('#main-form').on('change', '#items-list .pck-itm', function(event) {
		var idx = $('.pck-itm').index(this);
		var list = TabToVar();
		var _this = this;
		var oldValue = list.type[idx];

		list.type[idx] = parseInt($(this).val());
		list.dry[idx] = $('#items-list input[name="type"][value="dry-cleaning"]').eq(idx).prop('checked');
		
		$('.pck-itm').each(function(i, el) {
			if(list.type[idx] > 0) {
				$(el).children('option[value=' + list.type[idx] + ']').prop('disabled', true)
			}
			if(oldValue > 0) {
				$(el).children('option[value="' + oldValue + '"]').prop('disabled', false);
			}
		});

		if(allUsed()) {
			$('#items-list').append(getRowDefaultHTML());
		}
		updateStuff();
	});

	$('#main-form').on('mousedown', '#men', function() {
		if($(this).attr('class') != 'active') {
			$(activeTab).attr('class', '');
			$(this).attr('class', 'active');
			activeTab = '#men';
			listItems();
		}
	});
	$('#main-form').on('mousedown', '#women', function() {
		if($(this).attr('class') != 'active') {
			$(activeTab).attr('class', '');
			$(this).attr('class', 'active');
			activeTab = '#women';
			listItems();
		}
	});
	$('#main-form').on('mousedown', '#kids', function() {
		if($(this).attr('class') != 'active') {
			$(activeTab).attr('class', '');
			$(this).attr('class', 'active');
			activeTab = '#kids';
			listItems();
		}
	});
	$('#main-form').on('mousedown', '#house', function() {
		if($(this).attr('class') != 'active') {
			$(activeTab).attr('class', '');
			$(this).attr('class', 'active');
			activeTab = '#house';
			listItems();
		}
	});
});