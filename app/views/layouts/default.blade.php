<!doctype html>
<html>
	<head>
		@include('includes.head')
	</head>

	<body>
		<div id="header">
			@include('includes.header')
		</div>
		<div id="mailResponse"></div>

		@yield('content')

		@include('includes.footer')
	</body>
	<link rel="stylesheet" type="text/css" href="assets/datetimepicker/jquery.datetimepicker.css"/ >
	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
</html>