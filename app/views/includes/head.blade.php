
<meta charset="UTF-8">
<meta name="description" content="Washing services">
<meta name="author" content="Yiin">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<title>Wash-website</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.0/sandstone/bootstrap.min.css" rel="stylesheet"> -->
<link href="/assets/css/base.css" rel="stylesheet">
<link href="/assets/css/navbar-static-top.css" rel="stylesheet">

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript" src="/assets/js/core.js"></script>