<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Dusk Cleaners</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <style type="text/css">
        .navbar-right {
          padding-right: 80px;
        }
      </style>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="about">About Us</a></li>
        <li><a href="contact">Contact</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>