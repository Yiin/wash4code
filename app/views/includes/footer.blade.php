    <div class="table-layout footer">
      <div class="table-cell">
        <ul class="list-inline">
          <li><a href="/about">About Us</a></li>
          <li><a href="/contact">Contact</a></li>
        </ul>
        <div class="mission">
          <span class="fa fa-quote-left pull-left"></span>
          <p>Our mission is to make homes happier—whether it's giving people back time to do things at home they love, or providing opportunities for others that support their homes.</p>
        </div>
      </div>
    </div>