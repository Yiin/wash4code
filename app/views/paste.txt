						





















						<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">Book appointment</h4>
										<div class="col-xs-9">
                            				<div class="progress-bar-section">
												<hr>
												<ul class="list-inline">
													<li id="info-pane-person-icon" data-step="1" class="progress-bars step-1 active access-approved">
														<span class="active"></span>
														<p>Your Laundry</p>
													</li>
													<li id="contact-pane-icon" data-step="2" class="progress-bars step-2">
														<span></span>
														<p>Contact Info</p>
													</li>
													<li id="review-order-pane-icon" data-step="3" class="progress-bars step-3">
														<span></span>
														<p>Review Order</p>
													</li>
												</ul>
                            				</div><!-- /.progress-bar-section -->
                        				</div>
									</div>
									<div class="modal-body">
										<form class="form-horizontal" role="form">
											<div class="form-group">
												<ul class="nav nav-tabs" role="tablist">
													<label id="laundry-types">Laundry types</label>
													<li role="presentation" class="active" id="men"><a href="#">Men <span class="badge">12</span></a></li>
													<li role="presentation" id="women"><a href="#">Women <span class="badge">15</span></a></li>
													<li role="presentation" id="kids"><a href="#">Kids <span class="badge">4</span></a></li>
													<li role="presentation" id="house"><a href="#">House <span class="badge">38</span></a></li>
												</ul>
											</div>
											<div id="items-list">
												<div class="form-group">
													<label class="control-label col-sm-1" for="item">Item:</label>
													<div class="col-sm-5">
														<input type="phone" class="form-control item" placeholder="Enter item name">
													</div>
													<label class="control-label col-sm-1" for="item-count">Count:</label>
													<div class="col-sm-4">
														<input type="phone" class="form-control item-count" placeholder="Enter item count">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-1" for="item">Item:</label>
													<div class="col-sm-5">
														<input type="phone" class="form-control item" placeholder="Enter item name">
													</div>
													<label class="control-label col-sm-1" for="item-count">Count:</label>
													<div class="col-sm-4">
														<input type="phone" class="form-control item-count" placeholder="Enter item count">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-1" for="item">Item:</label>
													<div class="col-sm-5">
														<input type="phone" class="form-control item" placeholder="Enter item name">
													</div>
													<label class="control-label col-sm-1" for="item-count">Count:</label>
													<div class="col-sm-4">
														<input type="phone" class="form-control item-count" placeholder="Enter item count">
													</div>
												</div>
											</div>
											<div class="form-group">        
												<div>
													<button id="next-step" type="button" data-loading-text="Loading..." class="btn btn-success" onClick="showPrompt()">Enter contact details</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>