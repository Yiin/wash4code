@extends('layouts.default')
@section('content')

<div class="row clear" style="padding-bottom: 30px;">
  <div class=".col-xs-6 .col-lg-4 .col-md-5">
  	<div class="container">
      <h1>About Us</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus ligula, dapibus ut tempor ac, eleifend ut velit. Nam at quam sed libero aliquam placerat. Sed dignissim sit amet massa quis finibus. Integer blandit, lorem quis convallis consectetur, urna turpis ullamcorper massa, sit amet laoreet metus ante et augue. Quisque in ex in dolor ultricies malesuada ut a quam. Curabitur vehicula, massa ut bibendum sagittis, dui arcu cursus quam, at semper lorem tellus sed tortor. Ut id eros a ipsum auctor accumsan. Aliquam pretium dolor vel sollicitudin bibendum. Ut sed odio vitae ante placerat egestas ac quis nisi.</p>

      <p>Etiam ac condimentum dolor. Nam sodales malesuada magna id volutpat. Nulla nisl est, consectetur et felis vel, mollis volutpat nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In imperdiet lectus at odio dictum, eget posuere augue lobortis. Maecenas ac consectetur lectus. Cras tempus enim quis massa congue laoreet. Morbi pretium arcu ac vulputate malesuada. Mauris nibh dui, mollis vitae pellentesque id, hendrerit eget elit.</p>
      
      <p>Praesent quis lacinia justo, et dapibus velit. Nullam accumsan interdum sagittis. Etiam scelerisque interdum gravida. Proin non orci tempor, ullamcorper purus non, posuere justo. Suspendisse potenti. Suspendisse venenatis mauris id porta ultricies. Donec ipsum nisi, imperdiet eu ultrices in, suscipit ac odio. Mauris vitae velit augue.</p>

      <p>Duis a enim eget ante laoreet blandit nec id arcu. Ut semper arcu accumsan porttitor mollis. Praesent luctus dignissim ante, et cursus nisl iaculis sed. Nam pharetra, ex et blandit ultrices, est ipsum bibendum quam, non mollis nisi nulla sit amet lorem. Mauris porttitor, turpis in molestie rutrum, sapien tortor hendrerit orci, ut venenatis erat elit id augue. Vestibulum semper rutrum turpis nec vestibulum. Pellentesque dignissim risus orci, sed auctor nisl venenatis non. Vivamus ultricies, felis quis tempus dapibus, justo purus fermentum ex, vel maximus lacus purus in mi. Nunc cursus varius lorem, at consequat neque consectetur in.</p>

      <p>Quisque semper lorem ac lectus mattis malesuada. Curabitur purus massa, blandit vel urna ac, finibus vestibulum turpis. Pellentesque tincidunt porta ipsum quis rhoncus. Quisque felis nunc, suscipit at egestas eget, dictum eget dui. Ut id odio gravida, ultricies purus sit amet, finibus augue. Nullam sed dignissim justo. Nunc a odio tellus. Quisque condimentum lacinia arcu, sit amet pulvinar nunc finibus vitae.</p>
    </div>
  </div>
</div>
@stop