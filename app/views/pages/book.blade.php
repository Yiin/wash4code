@extends('layouts.default')
@section('content')

<div class="steps">
	<div class="container">
    <div class="row" style="display:inline-block">
      <h4 class="modal-title" id="myModalLabel" unselectable='on' onselectstart='return false;' onmousedown='return false;'>Current step</h4>
      <div class="col-xs-10" id="main_body_selection">
        <div class="progress-bar-section">
          <ul class="list-inline steps">
            <li id="pick-clothes-pane-icon" data-step="1" class="progress-bars step-1 active access-approved">
              <span class="active"></span>
              <p>Pick Clothes</p>
            </li>
            <li id="extra-info-pane-icon" data-step="2" class="progress-bars step-2">
              <span></span>
              <p>Tag the Clothes</p>
            </li>
            <li id="iron-pane-icon" data-step="3" class="progress-bars step-2">
              <span></span>
              <p>Iron Clothes</p>
            </li>
            <li id="contact-pane-icon" data-step="4" class="progress-bars step-2">
              <span></span>
              <p>Contact Info</p>
            </li>
            <li id="date-pane-icon" data-step="5" class="progress-bars step-2">
              <span></span>
              <p>Time & Date</p>
            </li>
            <li id="pay-pane-icon" data-step="6" class="progress-bars step-2">
              <span></span>
              <p>Payment type</p>
            </li>
            <!--<li id="review-order-pane-icon" data-step="7" class="progress-bars step-3">
              <span></span>
              <p>Review Order</p>
            </li>-->
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container" style="margin-bottom: 165px; margin-top: 25px; margin-left: 50px;">
  <form class="form-horizontal" role="form">
    <div id="main-form">
    </div>
    <div class="form-group">
      <div>
        <button id="prev-step" type="button" data-loading-text="Loading..." class="btn">Back</button>
        <button id="next-step" type="button" data-loading-text="Loading..." class="btn btn-success">Next</button>
      </div>
      <div>
      </div>
    </div>
  </form>
</div>
@stop