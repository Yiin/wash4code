@extends('layouts.default')
@section('content')

<div class="col-md-8 col-xs-12 front-page-body">
	<div class="pull-right hidden-xs">
		<img src="/assets/images/front-page-girl.jpg" alt="Satisfied Customer after Our Washing Services" class="front-page-photo">
	</div>
	<div class="front-page-text">
		<h3>One stop destination for drycleaning and Laundry</h3>
		<h3>We take extra care of your clothes!</h3>
		<section class="cta-section">
			<div class="cta-inner">
				<a class="btn btn-lg btn-cta" type="button" href="/book">Book now!</a>
			</div>
		</section>
	</div>
</div>
@stop