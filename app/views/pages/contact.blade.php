@extends('layouts.default')
@section('content')

<div class="row clear" style="padding-bottom: 30px;">
  <div class=".col-xs-6 .col-lg-4 .col-md-5">
  	<div class="container">
      <h1>Our Contacts</h1>
      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus ligula, dapibus ut tempor ac, eleifend ut velit.</h5>
      <address>
	    <strong>Number: </strong> (123) 456-7890<br>
	    <strong>E-mail: </strong><a href="mailto:#">first.last@example.com</a>
	  </address>
    </div>
  </div>
</div>
@stop